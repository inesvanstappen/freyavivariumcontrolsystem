#!/bin/bash

#	Run this script to install the Freya system
#	author: Tom 'SpuQ' Santens

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

FREYADIR="/opt/Freya/"

echo "+++ installing Freya"

# change directory to the directory of this script
echo "+ changing working directory to $(dirname "$0")";
cd "$(dirname "$0")"

# Check for NPM
echo -ne "+ checking for npm... "
if [[ $(which npm &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
	echo -e "\e[92m installed \e[39m"
else
	echo -e "\e[91m not installed \e[39m"
	echo "+ installing npm"
	apt-get -y -q install npm
fi

# Check for NodeJS -TODO requires an update...
echo -ne "+ checking for NodeJS... "
if [[ $(which nodejs &>/dev/null && echo 1 || echo 0) -ne 0 ]]; then
	echo -e "\e[92m installed \e[39m"
else
	echo -e "\e[91m not installed \e[39m"
	echo "+ installing NodeJS"
	curl -sL https://deb.nodesource.com/setup_4.x | sudo bash -
	apt-get -y -q install nodejs
fi

## FreyaReloaded app ##

# Create install directory
echo -ne "+ creating directory '$FREYADIR'... "
mkdir $FREYADIR
echo -e "\e[92mdone\e[39m"

# Copy files to install directory
echo -ne "+ copying files to '$FREYADIR'... "
cp -r ./* $FREYADIR
echo -e "\e[92mdone\e[39m"

# Install application dependencies
echo -ne "+ installing dependencies... "
cd $FREYADIR
npm install -y --silent
echo -e "\e[92mdone\e[39m"

# autostart line in /etc/rc.local
echo -ne "+ adding startup line in '/etc/rc.local'... "
sed -i -e '$i \# Freya startup' /etc/rc.local
sed -i -e '$i \sh /opt/Freya/start.sh' /etc/rc.local
echo -e "\e[92mdone\e[39m"

echo -e "\e[92mall done! \e[39m"
exit 0;

