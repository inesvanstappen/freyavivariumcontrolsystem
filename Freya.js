/*
 *	Freya.js
 *	TODO some explanatory text
 */

const fs = require('fs');
const qdevice = require('qdevice');

const webui = require('./webui');
const CircadianSchedule = require('./CircadianSchedule');
const TemperatureController = require('./TemperatureController');
const LightingController = require('./LightingController');
const HumidityController = require('./HumidityController');

var configfile = './config/climatecore.conf';	// default config file

// read config file path from argument, otherwise use default config file
if( process.argv[2] != null ) configfile = process.argv[2];
try{
	var settings = JSON.parse(cleanString(fs.readFileSync(configfile, 'utf8')));
} catch(err){
	console.error("corrupt config file; exit");
}

// remove unwanted characters from a string
// (used for e.g. cleaning up the UTF-8 BOM from a file)
// TODO move this function to other file (utils.js?)
function cleanString(input) {
	var output = "";
	for (var i=0; i<input.length; i++) {
		if (input.charCodeAt(i) <= 127) {
			output += input.charAt(i);
		}
	}
	return output;
}

var circadianSchedule = new CircadianSchedule( settings );

var temperatureController = new TemperatureController();
var lightingController = new LightingController();
var humidityController = new HumidityController();

var powerSwitch = new qdevice("RelayModule_1");
var sensor = new qdevice("freyaSensor_1");
var noSensorMode = false;

/* Circadian Rythme */
circadianSchedule.on('newTimeOfDay', function( timeOfDay ){

	console.log("----------- new settings -----------");
	console.log("timeOfDay: "+ timeOfDay.name);
	console.log("from "+timeOfDay.startHours+":"+timeOfDay.startMinutes);
	console.log("until "+timeOfDay.endHours+":"+timeOfDay.endMinutes);
	console.log("humidity:");
	console.log("\trainInterval: "+timeOfDay.humidity.rainInterval+" [mins]");
	console.log("\trainDuration: "+timeOfDay.humidity.rainDuration+" [sec]");
	console.log("\tminRhumidity: "+timeOfDay.humidity.minRhumidity+" [%]");
	console.log("\tmaxRhumidity: "+timeOfDay.humidity.maxRhumidity+" [%]");
	console.log("lighting:");
	console.log("\tminIntensity: "+timeOfDay.lighting.minIntensity+" [%]");
	console.log("\tmaxIntensity: "+timeOfDay.lighting.maxIntensity+" [%]");
	console.log("temperature:");
	console.log("\tminTemperature: "+timeOfDay.temperature.minTemperature+" [deg.C]");
	console.log("\tmaxTemperature: "+timeOfDay.temperature.maxTemperature+" [deg.C]");
	console.log("-----------------------------------");

	// set webui "season" to "timeOfDay" until we've implemented season =D
	webui.setCurrentSeason(timeOfDay.name);

	lightingController.settings( timeOfDay.lighting.minIntensity, timeOfDay.lighting.maxIntensity);
	temperatureController.settings( timeOfDay.temperature.minTemperature, timeOfDay.temperature.maxTemperature );
	humidityController.settings( timeOfDay.humidity.rainInterval, timeOfDay.humidity.rainDuration, timeOfDay.humidity.minRhumidity, timeOfDay.humidity.maxRhumidity );
});

/* Lighting Controller Output */
lightingController.on("lights", function( data ){
	powerSwitch.send('CH1', data);
});

/* Humidity Controller Output */
humidityController.on("sprinklers", function( data ){
	powerSwitch.send('CH2', data);
});

/* Temperature Controller Output */
temperatureController.on("heater", function( data ){
	powerSwitch.send('CH3', data);
});

/* Sensor */
sensor.on('disconnected', function(){
	temperatureController.noSensor(true);
	humidityController.noSensor(true);
	lightingController.noSensor(true);

	console.log("sensor disconnected; switched to noSensorMode");
	noSensorMode = true;
});

sensor.on('connected', function(){
	noSensorMode = false;

	temperatureController.noSensor(false);
	humidityController.noSensor(false);
	lightingController.noSensor(false);

	webui.setSensorStatus("ok");
	console.log("sensor connected");
});

sensor.on('data', function( data ){
	if( data.signal == "humidity" ){
		humidityController.setCurrent( data.argument );
		webui.setCurrentHumidity( data.argument );
	}
	else if( data.signal == "lighting" ){
		lightingController.setCurrent( data.argument );
		webui.setCurrentLighting( data.argument );
	}
	else if (data.signal == "temperature" ){
		temperatureController.setCurrent( data.argument );
		webui.setCurrentTemperature( data.argument );
	}
});

powerSwitch.on("connected", function(){
	console.log("powerSwitch connected");
});

powerSwitch.on("disconnected", function(){
	console.log("powerSwitch disconnected - I've lost control");
});
