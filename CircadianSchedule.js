var EventEmitter = require('events').EventEmitter;
var util = require('util');

util.inherits(CircadianSchedule, EventEmitter);	// ClimateCore object inherits from EventEmitter, to use the .emit() function

module.exports = CircadianSchedule;

function CircadianSchedule( settings ){
	this.settings = settings;	// the whole ClimateCore settings object
	//console.log(JSON.stringify(this.settings));

	this.currentTimeOfYear = 0;	// settings for the current time of year
	this.currentTimeOfDay = 0;	// settings for the current time of day


	// Check the date every x time as long as the program runs
	setInterval(function( climatecore ){
		var date = new Date();
		checkTimeOfYear( climatecore, date.getDate(), date.getMonth()+1 );
		checkTimeOfDay( climatecore, date.getHours(), date.getMinutes() );
	},2000, this);

	/*	TimeOfDay (hh:mm) related functions */

	// check if the current time belongs in the current time of day,
	// if not, set the new time of day.
	function checkTimeOfDay( climatecore, hours, minutes ){
		if( !timeIsWithinBounds( hours, minutes, climatecore.currentTimeOfDay.startHours, climatecore.currentTimeOfDay.startMinutes, climatecore.currentTimeOfDay.endHours, climatecore.currentTimeOfDay.endMinutes) ){
			var newTimeOfDay = lookupTimeOfDay( climatecore, hours, minutes);
			if( newTimeOfDay != 0 ){
				setTimeOfDay( climatecore, newTimeOfDay );
			}
		}
	}

	// lookup in which time of day (from current time of year settings) the given hh:mm belongs.
	// returns the timeOfDay object, or 0 when nothing matched
	function lookupTimeOfDay( climatecore, hours, minutes){
		// check whether there is a current time of year set
		if( climatecore.currentTimeOfYear == 0 ){
			console.log("no currentTimeOfYear is set!");
			return -1;
		}
		
		for(var i = 0; i <= climatecore.currentTimeOfYear.dayCycle.length; i++) {
			var n = climatecore.currentTimeOfYear.dayCycle[i];
			if( timeIsWithinBounds(hours, minutes, n.startHours, n.startMinutes, n.endHours, n.endMinutes) ){
				return n;
			}
		}
		console.log("no time of day found for this time");
		return 0;
	}
	function timeIsWithinBounds(hours, minutes, startHours, startMinutes, endHours, endMinutes){
		if(startHours < endHours){
			if( (hours >= startHours) && (hours <= endHours) ){
				if( (hours == startHours && minutes >= startMinutes) || (hours == endHours && minutes <= endMinutes) || (hours > startHours) || (hours < endHours) ){
					return 1;
				}
			}
		}
		else {
			if( (hours >= startHours && hours <= 23) || (hours >= 0 && hours <= endHours) ){
				if( (hours == startHours && minutes >= startMinutes) || (hours == endHours && minutes <= endMinutes) || hours < startHours || hours > endHours ){
					return 1;
				}
			}
		}
		return 0;
	}

	function setTimeOfDay( climatecore, timeOfDay ){
		climatecore.currentTimeOfDay = timeOfDay;
		climatecore.emit('newTimeOfDay', timeOfDay);
	}

	/*	TimeOfYear (day/month) related functions */

	// check whether the current date belongs in the current time of year,
	// if not, set the new time of year.
	function checkTimeOfYear( climatecore, day, month ){
		if( !dateIsWithinBounds(day, month, climatecore.currentTimeOfYear.startDay, climatecore.currentTimeOfYear.startMonth, climatecore.currentTimeOfYear.endDay, climatecore.currentTimeOfYear.endMonth ) ){
			var newTimeOfYear = lookupTimeOfYear( climatecore, day, month);
			if( newTimeOfYear != 0 ){
				setTimeOfYear( climatecore, newTimeOfYear );
			}
		}
	}

	// lookup in which time of year (from settings) the given day/month belongs.
	// returns the timeOfYear object, or 0 when nothing matched
	function lookupTimeOfYear( climatecore, day, month){
		for(var i = 0; i < climatecore.settings.length; i++) {
			//console.log("=== checking element "+i+"/"+climatecore.settings.length+" ===");
     			var n = climatecore.settings[i];
     			if( dateIsWithinBounds(day, month, n.startDay, n.startMonth, n.endDay, n.endMonth) ){
         			return n;
      			}
   		}
		console.log("no timeOfYear found for this date");
		return 0;
	}

	// checks whether a date is within the boundaries of the given start and end date
	function dateIsWithinBounds( day, month, startDay, startMonth, endDay, endMonth){
		if( startMonth <= endMonth ){
			if( (month >= startMonth) && (month <= endMonth) ){
				if( (month == startMonth && day >= startDay) || (month == endMonth && day <= endDay) || (month > startMonth) || (month < endMonth) ){
					return 1;
				}
			}
		}
		else {
			if( (month >= startMonth && month <= 12) || (month >= 1 && month <= endMonth) ){
				if( (month == startMonth && day >= startDay) || (month == endMonth && day <= endDay) || month < startMonth || month > endMonth ){
					return 1;
				}
			}
		}
		return 0;
	}

	function setTimeOfYear( climatecore, timeOfYear ){
		climatecore.currentTimeOfYear = timeOfYear;
		climatecore.emit('newTimeOfYear', timeOfYear);
	}
}		
