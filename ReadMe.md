![Freya logo](images/Freya_logo.png)

Every lifeform has its own environmental requirements that it needs to thrive. When no suitable environment is present, you can create it with Freya. Freya is an open source control system for *coding climate* in your vivarium; so you can, for example, get the exotic spider your girlfriend always dreamed of and make it feel at home, or grow potatoes on Mars. Your imagination is the only limit.

### Key features
*   **Web interface** for configuring and monitoring Freya
*   Configurable **seasons** with **day and night** cycles
*   Controllers for **lights**, **sprinklers** and **heater**

## Hardware
*   A Linux computer (e.g. Raspberry Pi 3 or higher)
*   The [Sensor Module](https://www.tindie.com/products/spuq/temperaturerhumidityrlight-sensor-module/) for sensing the environment's temperature, humidity and lighting
*   The [4-Channel Powerswitch Module](https://www.tindie.com/products/spuq/4-channel-powerswitch-module/) for controlling the lights, heater and sprinklers

>:information_source: You can implement your own hardware modules for adding custom features to Freya.
## Install / Uninstall
You need a Linux system (e.g. Raspberry Pi with Raspbian) dedicated to managing your vivarium. I recommend to configure a static IP, and to make sure the timezone is correct. Installing Freya requires an internet connection.

### Install
Open a terminal on, or SSH into your Linux system, and download and install this software
```bash
wget https://gitlab.com/SpuQ/freyavivariumcontrolsystem/-/archive/master/freyavivariumcontrolsystem-master.tar
tar -xvf freyavivariumcontrolsystem-master.tar
cd freyavivariumcontrolsystem-master
sudo sh install.sh
```
When all is done, reboot your system.
```bash
sudo shutdown -r now
```

### Uninstall
Navigate to the installation directory of the Freya system, and run the uninstall script.
```bash
cd /opt/Freya/
sudo sh uninstall.sh
```
## Contribute
There's lots of room for improvement, and plenty of possibilities to grow the system. Feel like playing along? Go for it, and let me know!
