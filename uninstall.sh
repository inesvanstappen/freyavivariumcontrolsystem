#!/bin/bash

#	Run this script to uninstall the Freya system
#	author: Tom 'SpuQ' Santens

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

FREYADIR="/opt/Freya/"

echo "+++ uninstalling Freya"

## FreyaReloaded ##
echo -ne "+ removing '$FREYADIR'... "
rm -rf $FREYADIR
echo -e "\e[92mdone\e[39m"

echo -ne "+ removing lines from '/etc/rc.local'... "
sed -i '/Freya/d' /etc/rc.local
echo -e "\e[92mdone\e[39m"

echo -e "\e[92mall done! \e[39m"
exit 0;
