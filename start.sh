#!/bin/bash

#	This script starts FreyaReloaded application
#	author: Tom 'SpuQ' Santens


# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# change directory to the directory of this script
cd "$(dirname "$0")"

nodejs Freya.js &

exit 0;
