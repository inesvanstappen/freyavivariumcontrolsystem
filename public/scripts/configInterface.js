var timesOfYear = [];	// contains array of timeOfYear elements
var timesOfYearList;
var addTimeOfYearButton;
var downloadConfigButton;

$(document).ready(function() {
	timesOfYearList = $('#timeOfYearList');
	addTimeOfYearButton = $('#addTimeOfYearButton');
	downloadConfigButton = $('#downloadConfigButton');

	addTimeOfYearButton.click(function(){
		addBlankTimeOfYearElement();
		refreshTimeOfYearList();
	});

	// load current settings
	getSettings();
	
	// save settings button
	downloadConfigButton.click(function(){
		postSettings();
	});

	//refreshTimeOfYearList();
});


// visual objects

function refreshTimeOfYearList(){
	console.log("refreshing timeOfYearList");
	// clear the list
	timesOfYearList.empty();

	if( timesOfYear.length <= 0){
		timesOfYearList.append('<div class="alert alert-danger col-sm-12"><p><i class="fa fa-exclamation-triangle"></i> Freya needs at least one season!</p></div>');
		}
	// for each timesOfYear element
	timesOfYear.forEach( function(timeOfYear){
		timesOfYearList.append('<li class="list-group-item" id="'+timeOfYear.uuid+'"></li>');
		var timeOfYearItem = $("#"+timeOfYear.uuid);

		//timeOfYearItem.append('<div class="card">');
		timeOfYearItem.append('<div class="card-header">');
		timeOfYearItem.append('<div >');
		timeOfYearItem.append('<h2 class="col-lg-12 well well-sm card-title">'+timeOfYear.name+' <button class="btn btn-danger" onclick="removeTimeOfYearElement(\''+timeOfYear.uuid+'\')" style="float: right;"><i class="fa fa-trash"></i></button></h2>');
		timeOfYearItem.append('</div>'); // end card header
		timeOfYearItem.append('<div class="card-body">'); // card body
		timeOfYearItem.append('<div class="form-group row">');
		// TimeOfYear name
		timeOfYearItem.append('<div class="form-group">');
		timeOfYearItem.append('<label for="'+timeOfYear.uuid+'Name" class="col-sm-1 col-form-label">Name</label>');
		timeOfYearItem.append('<input type="text" id="'+timeOfYear.uuid+'Name" value="'+timeOfYear.name+'" onfocusout="saveTimeOfYearElement(\''+timeOfYear.uuid+'\')">');
		timeOfYearItem.append('</div>');
		// TimeOfYear start and end dates
		timeOfYearItem.append('<div class="form-group">');
		timeOfYearItem.append('<label for="'+timeOfYear.uuid+'startDay" class="col-sm-1 col-form-label">Start</label>');
		timeOfYearItem.append('<input type="number" min="1" max="31" id="'+timeOfYear.uuid+'StartDay" value="'+timeOfYear.startDay+'" onfocusout="saveTimeOfYearElement(\''+timeOfYear.uuid+'\')">/');
		timeOfYearItem.append('<input type="number" min="1" max="12" id="'+timeOfYear.uuid+'StartMonth" value="'+timeOfYear.startMonth+'" onfocusout="saveTimeOfYearElement(\''+timeOfYear.uuid+'\')">');
		timeOfYearItem.append('</div>');
		timeOfYearItem.append('<div class="form-group">');
		timeOfYearItem.append('<label for="'+timeOfYear.uuid+'EndDay" class="col-sm-1 col-form-label">End</label>');
		timeOfYearItem.append('<input type="number" min="1" max="31" id="'+timeOfYear.uuid+'EndDay" value="'+timeOfYear.endDay+'" onfocusout="saveTimeOfYearElement(\''+timeOfYear.uuid+'\')">/');
		timeOfYearItem.append('<input type="number" min="1" max="12" id="'+timeOfYear.uuid+'EndMonth" value="'+timeOfYear.endMonth+'" onfocusout="saveTimeOfYearElement(\''+timeOfYear.uuid+'\')">');
		timeOfYearItem.append('</div>');
		// List for TimesOfDay
		timeOfYearItem.append('<h3><i class="fa fa-clock"></i> Day schedule</h3>');
		timeOfYearItem.append('<ul id="'+timeOfYear.uuid+'TimesOfDay" class="list-group list-group-flush row"></ul>');

		timeOfYearItem.append('</div>'); // end card body
		timeOfYearItem.append('</div>'); // end card
		timeOfYearItem.append('</div>'); // end of timeOfYear element
		$("#"+timeOfYear.uuid).html($("#"+timeOfYear.uuid).html());

		var timesOfDayList = $('#'+timeOfYear.uuid+'TimesOfDay');
		// add warning if list is empty
		if( timeOfYear.dayCycle.length <= 0){
			timesOfDayList.append('<div class="alert alert-danger col-sm-12"><p><i class="fa fa-exclamation-triangle"></i> Each season must have at least one time of day!</p></div>');
		}
		for(var i = 0; i < timeOfYear.dayCycle.length; i++){
			timesOfDayList.append('<li class="list-group-item col-sm-6" id="'+timeOfYear.dayCycle[i].uuid+'"></li>');
			var timesOfDayListItem = $('#'+timeOfYear.dayCycle[i].uuid);
			timesOfDayListItem.append("<h3>"+timeOfYear.dayCycle[i].name+" <button class='btn btn-danger' onclick='removeTimeOfDayElement(\""+timeOfYear.uuid+"\",\""+timeOfYear.dayCycle[i].uuid+"\")' style='float: right;'><i class='fa fa-trash'></i></button></h3>");
			timesOfDayListItem.append('<div class="form-group row>'); // start form

			// name
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'Name" class="col-sm-2 col-form-label">Name</label>');
			timesOfDayListItem.append('<input type="text" id="'+timeOfYear.dayCycle[i].uuid+'Name" value="'+timeOfYear.dayCycle[i].name+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">');
			timesOfDayListItem.append('</div>');
			// start and end times
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'startHours" class="col-sm-2 col-form-label">Start</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="23" id="'+timeOfYear.dayCycle[i].uuid+'startHours" value="'+timeOfYear.dayCycle[i].startHours+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')" >:');
			timesOfDayListItem.append('<input type="number" min="0" max="59" id="'+timeOfYear.dayCycle[i].uuid+'startMinutes" value="'+timeOfYear.dayCycle[i].startMinutes+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'endHours" class="col-sm-2 col-form-label">End</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="23" id="'+timeOfYear.dayCycle[i].uuid+'endHours" value="'+timeOfYear.dayCycle[i].endHours+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">:');
			timesOfDayListItem.append('<input type="number" min="0" max="59" id="'+timeOfYear.dayCycle[i].uuid+'endMinutes" value="'+timeOfYear.dayCycle[i].endMinutes+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">');
			timesOfDayListItem.append('</div>');
			// controller settings
			timesOfDayListItem.append('<h4><i class="fa fa-lightbulb-o"></i> Lighting</h4>'); // lighting
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'minIntensity" class="col-sm-2 col-form-label">min</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="100" id="'+timeOfYear.dayCycle[i].uuid+'minIntensity" value="'+timeOfYear.dayCycle[i].lighting.minIntensity+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">%');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'maxIntensity" class="col-sm-2 col-form-label">max</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="100" id="'+timeOfYear.dayCycle[i].uuid+'maxIntensity" value="'+timeOfYear.dayCycle[i].lighting.maxIntensity+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">%');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<h4><i class="fa fa-thermometer-half"></i> Temperature</h4>'); // temperature
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'minTemperature" class="col-sm-2 col-form-label">min</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="100" id="'+timeOfYear.dayCycle[i].uuid+'minTemperature" value="'+timeOfYear.dayCycle[i].temperature.minTemperature+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">&deg;C');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'maxTemperature" class="col-sm-2 col-form-label">max</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="100" id="'+timeOfYear.dayCycle[i].uuid+'maxTemperature" value="'+timeOfYear.dayCycle[i].temperature.maxTemperature+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">&deg;C');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<h4><i class="fa fa-tint"></i> Humidity</h4>'); // humidity
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'minRhumidity" class="col-sm-2 col-form-label">min</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="100" id="'+timeOfYear.dayCycle[i].uuid+'minRhumidity" value="'+timeOfYear.dayCycle[i].humidity.minRhumidity+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">%');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'maxRhumidity" class="col-sm-2 col-form-label">max</label>');
			timesOfDayListItem.append('<input type="number" min="0" max="100" id="'+timeOfYear.dayCycle[i].uuid+'maxRhumidity" value="'+timeOfYear.dayCycle[i].humidity.maxRhumidity+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">%');
			timesOfDayListItem.append('</div>');

			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'rainInterval" class="col-sm-2 col-form-label">interval</label>');
			timesOfDayListItem.append('<input type="number" id="'+timeOfYear.dayCycle[i].uuid+'rainInterval" value="'+timeOfYear.dayCycle[i].humidity.rainInterval+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">minutes');
			timesOfDayListItem.append('</div>');
			timesOfDayListItem.append('<div class="form-group">');
			timesOfDayListItem.append('<label for="'+timeOfYear.dayCycle[i].uuid+'rainDuration" class="col-sm-2 col-form-label">duration</label>');
			timesOfDayListItem.append('<input type="number" id="'+timeOfYear.dayCycle[i].uuid+'rainDuration" value="'+timeOfYear.dayCycle[i].humidity.rainDuration+'" onfocusout="saveTimeOfDayElement(\''+timeOfYear.uuid+'\',\''+timeOfYear.dayCycle[i].uuid+'\')">seconds');
			timesOfDayListItem.append('</div>');
			
			timesOfDayListItem.append('</div>'); // end form
			timesOfDayListItem.append("<!-- <hr><button class='btn btn-danger' onclick='removeTimeOfDayElement(\""+timeOfYear.uuid+"\",\""+timeOfYear.dayCycle[i].uuid+"\")'><i class='fa fa-trash'></i> Delete item</button><button class='btn btn-success' onclick='saveTimeOfDayElement(\""+timeOfYear.uuid+"\",\""+timeOfYear.dayCycle[i].uuid+"\")'><i class='fa fa-save'></i> Save item</button>-->");
		}
		// add TimeOfDay button
		timesOfDayList.append("<button class='btn btn-primary' onclick='addTimeOfDayElement(\""+timeOfYear.uuid+"\")'>+ add new ...</button>");
		
	});
}

// add a new blanco timeOfYear element to the timesOfYear array
function addTimeOfYearElement( name, startDay, startMonth, endDay, endMonth ){
	timeOfYear={};

	timeOfYear.uuid = uuidv4();
	timeOfYear.name = name;
	timeOfYear.startDay = startDay;
	timeOfYear.startMonth = startMonth;
	timeOfYear.endDay = endDay;
	timeOfYear.endMonth = endMonth;
	timeOfYear.dayCycle = [];

	timesOfYear.push( timeOfYear );

	return timeOfYear.uuid;
}

function saveTimeOfYearElement( uuid ){
	for(var i = 0; i < timesOfYear.length; i++) {
	    if(timesOfYear[i].uuid == uuid) {

		timesOfYear[i].name = $("#"+timesOfYear[i].uuid+"Name").val();
		timesOfYear[i].startDay = parseInt($("#"+timesOfYear[i].uuid+"StartDay").val());
		timesOfYear[i].startMonth = parseInt($("#"+timesOfYear[i].uuid+"StartMonth").val());
		timesOfYear[i].endDay = parseInt($("#"+timesOfYear[i].uuid+"EndDay").val());
		timesOfYear[i].endMonth = parseInt($("#"+timesOfYear[i].uuid+"EndMonth").val());

		refreshTimeOfYearList();
		break;
		}
	}
}

function removeTimeOfYearElement( uuid ){
	console.log("removing: "+uuid);

	for(var i = 0; i < timesOfYear.length; i++) {
	    if(timesOfYear[i].uuid == uuid) {
		timesOfYear.splice(i, 1);
		refreshTimeOfYearList();
		break;
	}

}
	
}

function addBlankTimeOfYearElement(){
	var uuid = addTimeOfYearElement( "newSeason", 0, 0, 0, 0 );
	// add at least one timeOfDay...
	addTimeOfDayElement( uuid );
}

// add a new blanco timeOfYear element to the given timeOfYear element
function addTimeOfDayElement( uuid ){
	// search the timesOfYear array for timeOfYear with uuid
	var timeOfYear;

	for(var i = 0; i < timesOfYear.length; i++) {
		if(timesOfYear[i].uuid == uuid) {
			timeOfYear = timesOfYear[i];
			break;
		}
	}
	var timeOfDay = {};

	timeOfDay.uuid = uuidv4();
	timeOfDay.name = "name";
	timeOfDay.startHours = 0;
	timeOfDay.startMinutes = 0;
	timeOfDay.endHours = 0;
	timeOfDay.endMinutes = 0;

	timeOfDay.humidity = {};
	timeOfDay.temperature = {};
	timeOfDay.lighting = {};
	timeOfDay.lighting.minIntensity = 0;
	timeOfDay.lighting.maxIntensity = 0;
	timeOfDay.temperature.minTemperature = 0;
	timeOfDay.temperature.maxTemperature = 0;
	timeOfDay.humidity.minRhumidity = 0;
	timeOfDay.humidity.maxRhumidity = 0;
	timeOfDay.humidity.rainInterval = 0;
	timeOfDay.humidity.rainDuration = 0;

	timeOfYear.dayCycle.push( timeOfDay );
	refreshTimeOfYearList();
}

function removeTimeOfDayElement( toyuuid, toduuid){
	// first, find the time of year in which the time of day is
	var timeOfYear;

	for(var i = 0; i < timesOfYear.length; i++) {
		if(timesOfYear[i].uuid == toyuuid) {
			timeOfYear = timesOfYear[i];
			break;
		}
	}

	for(var i = 0; i < timeOfYear.dayCycle.length; i++) {
		if(timeOfYear.dayCycle[i].uuid == toduuid) {
			timeOfYear.dayCycle.splice(i, 1);
			refreshTimeOfYearList();
			break;
		}
	}
}

function saveTimeOfDayElement( toyuuid, toduuid){
	var timeOfYear;

	for(var i = 0; i < timesOfYear.length; i++) {
		if(timesOfYear[i].uuid == toyuuid) {
			timeOfYear = timesOfYear[i];
			break;
		}
	}

	for(var i = 0; i < timeOfYear.dayCycle.length; i++) {
		if(timeOfYear.dayCycle[i].uuid == toduuid) {

			timeOfYear.dayCycle[i].name = $("#"+timeOfYear.dayCycle[i].uuid+"Name").val();
			timeOfYear.dayCycle[i].startHours = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"startHours").val());
			timeOfYear.dayCycle[i].startMinutes = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"startMinutes").val());
			timeOfYear.dayCycle[i].endHours = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"endHours").val());
			timeOfYear.dayCycle[i].endMinutes = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"endMinutes").val());
			// lighting
			timeOfYear.dayCycle[i].lighting.minIntensity = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"minIntensity").val());
			timeOfYear.dayCycle[i].lighting.maxIntensity = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"maxIntensity").val());
			// temperature
			timeOfYear.dayCycle[i].temperature.minTemperature = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"minTemperature").val());
			timeOfYear.dayCycle[i].temperature.maxTemperature = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"maxTemperature").val());
			// humidity
			timeOfYear.dayCycle[i].humidity.minRhumidity = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"minRhumidity").val());
			timeOfYear.dayCycle[i].humidity.maxRhumidity = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"maxRhumidity").val());
			timeOfYear.dayCycle[i].humidity.rainDuration = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"rainDuration").val());
			timeOfYear.dayCycle[i].humidity.rainInterval = parseInt($("#"+timeOfYear.dayCycle[i].uuid+"rainInterval").val());

			refreshTimeOfYearList();
			break;
		}
	}
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function getSettings(){
	$.get(window.location.origin+'/api/settings', function( settings ){
		console.log(settings);
		timesOfYear = settings;
		refreshTimeOfYearList();
	}, 'json');
}

function postSettings(){
	var data = JSON.stringify(timesOfYear);
	console.log(data);
	//$.post(window.location.origin+'/api/settings', data, 'text/plain' );

	$.ajax({
		type: "POST",
		url: window.location.origin+"/api/settings",
		data: data,
		contentType:  "application/json; charset=utf-8",
		dataType: "json"
	});
}
