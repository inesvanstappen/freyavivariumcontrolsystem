var url = window.location.href;

window.onload = function() {
	var t = setInterval(update, 1000);

	initTempGraph();
	initHumGraph();
	initLightGraph();

	// TODO not so clean; fetch data on the right moment!
	setTimeout(function(){
		// set system UUID
		freya.getControllerUUID(function(data){
			$('#controllerUUID').html(data.uuid);
		});
	},2000);
};

/*
 *	Update function for main view
 */
function update() {
	// update mainView values
	freya.getCurrentTemperature(function(data) {
		$('#mainTemp').html( data.temperature + "&deg;C" );
	});

	freya.getCurrentLighting(function(data) {
		$('#mainLight').html( data.lighting + "%" );
	});

	freya.getCurrentHumidity(function(data) {
		$('#mainHum').html( data.humidity + "%" );
	});

	freya.getCurrentSeason(function(data) {
		$('#mainSeason').html( data.season +"" );
	});

	updateTemperatureStatus();
	updateHumidityStatus();
	updateLightingStatus();

	// refresh graphs if modal is open
	if( $('#modaltemperature').hasClass('in') ){
		refreshTempGraph();
	}

	if( $('#modalhumidity').hasClass('in') ){
		refreshHumGraph();
	}

	if( $('#modallighting').hasClass('in') ){
		refreshLightGraph();
	}

	// refresh devicelist
	if( $('#modalsysinfo').hasClass('in') ){
		freya.getDeviceList(function(data){
			$('#QcomDeviceList').empty();
			for(var i = 0; i < data.length; i++) {
				$('#QcomDeviceList').append("<li>"+data[i]+"</li>");
			}
		});
	}
}


/*
 *	modal open triggers 
 */
$('#modaltemperature').on('shown.bs.modal', function(e) {
	console.log("Modal: temperature is open");
	refreshTempGraph();
});

$('#modalhumidity').on('shown.bs.modal', function(e) {
	console.log("Modal: humidity is open");
	refreshHumGraph();
});

$('#modallighting').on('shown.bs.modal', function(e) {
	console.log("Modal: lighting is open");
	refreshLightGraph();
});

$('#modalseason').on('show.bs.modal', function(e) {
	console.log("Modal 'season' open");

	// TODO get settings
});


/*
 *	Status update functions
 */
function updateTemperatureStatus(){
   freya.getTemperatureStatus(function(data) {
      console.log("temperature status:"  + data.status + " - "+ data.message);
         switch(data.status) {
            case "ok":
               $('#temperature-status-ok').show();
               $('#temperature-status-warning').hide();
               $('#temperature-status-error').hide();

               $('#temperature-status-ok-modal').show();
               $('#temperature-status-warning-modal').hide();
               $('#temperature-status-error-modal').hide();
               break;
            case "warning":
               $('#temperature-status-ok').hide();
               $('#temperature-status-warning').show();
               $('#temperature-status-error').hide();

               $('#temperature-status-ok-modal').hide();
               $('#temperature-status-warning-modal').show();
               $('#temperature-status-error-modal').hide();
               break;
            case "error":
               $('#temperature-status-ok').hide();
               $('#temperature-status-warning').hide();
               $('#temperature-status-error').show();	

               $('#temperature-status-ok-modal').hide();
               $('#temperature-status-warning-modal').hide();
               $('#temperature-status-error-modal').show();		
               break;
	}

        $('#temperature-status').empty().append(data.status);
        $('#temperature-message').empty().append(data.message);
   });
}

function updateHumidityStatus(){
   freya.getHumidityStatus(function(data) {
      console.log("humidity status:"  + data.status + " - "+ data.message);
         switch(data.status) {
            case "ok":
               $('#humidity-status-ok').show();
               $('#humidity-status-warning').hide();
               $('#humidity-status-error').hide();

               $('#humidity-status-ok-modal').show();
               $('#humidity-status-warning-modal').hide();
               $('#humidity-status-error-modal').hide();
               break;
            case "warning":
               $('#humidity-status-ok').hide();
               $('#humidity-status-warning').show();
               $('#humidity-status-error').hide();

               $('#humidity-status-ok-modal').hide();
               $('#humidity-status-warning-modal').show();
               $('#humidity-status-error-modal').hide();
               break;
            case "error":
               $('#humidity-status-ok').hide();
               $('#humidity-status-warning').hide();
               $('#humidity-status-error').show();

               $('#humidity-status-ok-modal').hide();
               $('#humidity-status-warning-modal').hide();
               $('#humidity-status-error-modal').show();			
               break;
	}

	$('#humidity-status').empty().append(data.status);
	$('#humidity-message').empty().append(data.message);
   });
}

function updateLightingStatus(){
   freya.getLightingStatus(function(data) {
      console.log("lighting status:"  + data.status + " - "+ data.message);
         switch(data.status) {
            case "ok":
               $('#lighting-status-ok').show();
               $('#lighting-status-warning').hide();
               $('#lighting-status-error').hide();

               $('#lighting-status-ok-modal').show();
               $('#lighting-status-warning-modal').hide();
               $('#lighting-status-error-modal').hide();
               break;
            case "warning":
               $('#lighting-status-ok').hide();
               $('#lighting-status-warning').show();
               $('#lighting-status-error').hide();

               $('#lighting-status-ok-modal').hide();
               $('#lighting-status-warning-modal').show();
               $('#lighting-status-error-modal').hide();
               break;
            case "error":
               $('#lighting-status-ok').hide();
               $('#lighting-status-warning').hide();
               $('#lighting-status-error').show();

               $('#lighting-status-ok-modal').hide();
               $('#lighting-status-warning-modal').hide();
               $('#lighting-status-error-modal').show();	
               break;
	}

        $('#lighting-status').empty().append(data.status);
        $('#lighting-message').empty().append(data.message);
   });
}


/*
 *	Seasons save settings
 */
function saveAllSettings(){
	console.log("saving all settings");
	saveTemperatureSettings();
	saveHumiditySettings();
	saveLightingSettings();
}


/*
 *	Settings
 */
function getTemperatureSettings() {
  freya.getTemperatureSettings(function(data) {
    console.log("Temperature settings fetched: \nminimum: "+ data.minimum +"\nmaximum: "+ data.maximum );
    if (typeof data.minimum != 'undefined') $('#temperature-minimum').val(data.minimum);
    if (typeof data.maximum != 'undefined') $('#temperature-maximum').val(data.maximum);
  });

	// TODO draw graph guides
}

function getHumiditySettings() {
  freya.getHumiditySettings(function(data) {
    console.log("Humidity settings fetched: \nminimum: "+ data.minimum +"\nmaximum: "+ data.maximum +"\nduration: "+ data.duration +"\ninterval: "+ data.interval );
    if (typeof data.minimum != 'undefined') $('#humidity-minimum').val(data.minimum);
    if (typeof data.maximum != 'undefined') $('#humidity-maximum').val(data.maximum);
    if (typeof data.interval != 'undefined') $('#humidity-interval').val(data.interval);
    if (typeof data.duration != 'undefined') $('#humidity-duration').val(data.duration);
  });
}

function getLightingSettings() {
  freya.getLightingSettings(function(data) {
    console.log("Lighting settings fetched: \nsunrise: "+ data.sunrise +"\nsunset: "+ data.sunset );
    if (typeof data.sunrise != 'undefined') $('#lighting-sunrise').val(data.sunrise);
    if (typeof data.sunset != 'undefined') $('#lighting-sunset').val(data.sunset);
  });
}

function saveTemperatureSettings() {
  console.log("saving temperature settings: \nminimum: "+$('#temperature-minimum').val()+"\nmaximum: "+ $('#temperature-maximum').val() );
  data = {};
  // TODO make user input fool-proof before posting to system - Them users, you know how they can be
  data.minimum = $('#temperature-minimum').val();
  data.maximum = $('#temperature-maximum').val();

  freya.setTemperatureSettings(data, function() {
    console.log("Temperature settings saved");
  });
}

function saveHumiditySettings() {
  console.log("saving humidity settings: \nminimum: "+$('#humidity-minimum').val()+"\nmaximum: "+ $('#humidity-maximum').val() +"\nduration: "+$('#humidity-duration').val()+"\ninterval: "+ $('#humidity-interval').val() );
  data = {};
  // TODO make user input fool-proof before posting to system
  data.minimum = $('#humidity-minimum').val();
  data.maximum = $('#humidity-maximum').val();
  data.interval = $('#humidity-interval').val();
  data.duration = $('#humidity-duration').val();

  freya.setHumiditySettings(data, function() {
    console.log("Humidity settings saved");
  });
}

function saveLightingSettings() {
  console.log("saving lighting settings: \nsunrise: "+$('#lighting-sunrise').val()+"\nsunset: "+ $('#lighting-sunset').val() );
  data = {};
  // TODO make user input fool-proof before posting to system
  data.sunrise = $('#lighting-sunrise').val();
  data.sunset = $('#lighting-sunset').val();

  freya.setLightingSettings(data, function() {
    console.log("Lighting settings saved");
  });
}


/*
 *	testing
 */
function testLighting() {
  data = {};
  freya.testLighting(data, function() {
    console.log("Testing lighting controller");
  });
}

function testHumidity() {
  data = {};
  freya.testHumidity(data, function() {
    console.log("Testing humidity controller");
  });
}

function testTemperature() {
  data = {};
  freya.testTemperature(data, function() {
    console.log("Testing temperature controller");
  });
}


/*
 *	Graphs
 */
 
/*	Temperature Graph	*/
var tempChart;
var tempGraph;
var tempData;
var tempValueAxis;

function initTempGraph() {
  tempChart = new AmCharts.AmSerialChart();
  tempChart.dataProvider = tempData;
  tempChart.categoryField = "timestamp";

  tempGraph = new AmCharts.AmGraph();

  tempValueAxis = new AmCharts.ValueAxis();
  tempValueAxis.axisThickness = 0;
  tempValueAxis.labelsEnabled = true;
  tempValueAxis.labelFunction = function(value){return value + "ºC"};
  tempValueAxis.position = "left";  
  tempValueAxis.labelOffset = 15
  tempChart.addValueAxis(tempValueAxis);

  tempValueAxis.minimum = 35;
  tempValueAxis.maximum = 15;

  var categoryAxis = new AmCharts.CategoryAxis();
  categoryAxis.labelsEnabled = true;
  categoryAxis.axisThickness = "0";
  categoryAxis.parseDates  = false;
  categoryAxis.categoryFunction = function(value){return AmCharts.formatDate(new Date(value), "HH:NN");}

  tempChart.categoryAxis = categoryAxis;

  tempGraph.valueField = "temperature";
  tempGraph.type = "line";
  tempChart.addGraph(tempGraph);

  tempGraph.fillAlphas = 0; // or delete this line, as 0 is default
  tempGraph.bullet = "none";
  tempGraph.lineColor = "#353333";
  tempGraph.lineThickness = "2";

  tempChart.write('temperature-graph');

  // testing purpose
  //setTempGuides(20, 30);
}

function setTempGuides(minimum, maximum) {
  // TODO get values from back-end
  tempValueAxis.guides = [{
    "value": maximum,
    "lineAlpha": 0.8,
    "lineColor": "#c00",
    "lineThickness": 1,
    "label": "max",
    "position": "right"
  }, {
    "value": minimum,
    "lineAlpha": 0.8,
    "lineColor": "#c00",
    "lineThickness": 1,
    "label": "min",
    "position": "right"
  }];
}

function refreshTempGraph() {
  freya.getTemperatureGraphData(function(graphdata) {
    tempData = graphdata;
    tempChart.dataProvider = tempData;
    tempChart.validateData();
  });
}


/*	Humidity Graph		*/
var humChart;
var humGraph;
var humData;
var humValueAxis;

function initHumGraph() {
  humChart = new AmCharts.AmSerialChart();
  humChart.dataProvider = humData;
  humChart.categoryField = "timestamp";

  humGraph = new AmCharts.AmGraph();

  humValueAxis = new AmCharts.ValueAxis();
  humValueAxis.axisThickness = 0;
  humValueAxis.labelsEnabled = true;
  humValueAxis.dashLength = 0;
  humValueAxis.labelFunction = function(value){return value + "%"};
  humValueAxis.minimum = 0;
  humValueAxis.maximum = 100;
  humValueAxis.position = "left";  
  humValueAxis.labelOffset = 15

  humChart.addValueAxis(humValueAxis);

  var categoryAxis = new AmCharts.CategoryAxis();
  categoryAxis.labelsEnabled = true;
  categoryAxis.axisThickness = "0";
  categoryAxis.parseDates  = false;
  categoryAxis.categoryFunction = function(value){return AmCharts.formatDate(new Date(value), "HH:NN");}

  humChart.categoryAxis = categoryAxis;

  humGraph.valueField = "humidity";
  humGraph.type = "line";
  humChart.addGraph(humGraph);

  humGraph.fillAlphas = 0; // or delete this line, as 0 is default
  humGraph.bullet = "none";
  humGraph.lineColor = "#353333";
  humGraph.lineThickness = "2";

  humChart.write('humidity-graph');

  // testing purpose
  //setHumGuides(20, 90);
}

function setHumGuides(minimum, maximum) {
  // guides
  humValueAxis.guides = [{
    "value": maximum,
    "lineAlpha": 0.8,
    "lineColor": "#c00",
    "lineThickness": 1,
    "label": "max",
    "position": "right"
  }, {
    "value": minimum,
    "lineAlpha": 0.8,
    "lineColor": "#c00",
    "lineThickness": 1,
    "label": "min",
    "position": "right"
  }];
}

function refreshHumGraph() {
  freya.getHumidityGraphData(function(graphdata) {
    humData = graphdata;
    humChart.dataProvider = humData;
    humChart.validateData();
  });
}


/*	Lighting Graph		*/
var lightChart;
var lightGraph;
var lightData;
var lightValueAxis;

function initLightGraph() {
  lightChart = new AmCharts.AmSerialChart();
  lightChart.dataProvider = lightData;
  lightChart.categoryField = "timestamp";

  lightGraph = new AmCharts.AmGraph();

  lightValueAxis = new AmCharts.ValueAxis();
  lightValueAxis.axisThickness = 0;
  lightValueAxis.labelsEnabled = true;
  lightValueAxis.dashLength = 0;
  lightValueAxis.labelFunction = function(value){return value + "%"};
  lightChart.addValueAxis(lightValueAxis);

  lightValueAxis.minimum = "0";
  lightValueAxis.maximum = "100";

  var categoryAxis = new AmCharts.CategoryAxis();
  categoryAxis.labelsEnabled = true;
  categoryAxis.axisThickness = "0";
  categoryAxis.parseDates  = false;
  categoryAxis.categoryFunction = function(value){return AmCharts.formatDate(new Date(value), "HH:NN");}

  lightChart.categoryAxis = categoryAxis;

  lightGraph.valueField = "lighting";
  lightGraph.type = "line";
  lightChart.addGraph(lightGraph);

  lightGraph.fillAlphas = 0; // or delete this line, as 0 is default
  lightGraph.bullet = "none";
  lightGraph.lineColor = "#353333";
  lightGraph.lineThickness = "2";

  lightChart.write('lighting-graph');
}

function setLightGuides(minimum, maximum) {
  // guides
  lightValueAxis.guides = [{
    "value": maximum,
    "lineAlpha": 0.8,
    "lineColor": "#c00",
    "lineThickness": 1,
    "label": "max",
    "position": "right"
  }, {
    "value": minimum,
    "lineAlpha": 0.8,
    "lineColor": "#c00",
    "lineThickness": 1,
    "label": "min",
    "position": "right"
  }];
}

function refreshLightGraph() {
  freya.getLightingGraphData(function(graphdata) {
    lightData = graphdata;
    lightChart.dataProvider = lightData;
    lightChart.validateData();
  });
}
