/*	Freya API (Freya-PP)
 *	Wrapper around Freya's REST api.
 *
 *	written on 08/05/2017 by Tom 'SpuQ' Santens
 */
 
freya = {
	/*
	*	Temperature
	*/
	//	get the current temperature
	//	data: { "temperature" : 21.6 }
	getCurrentTemperature: function(callback) {
		$.get("/api/temperature/current", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the temperature graph data
	//	data: [{"temperature": 18.8 ,"timestamp":1493012412553}]
	getTemperatureGraphData: function(callback) {
		$.get("/api/temperature/graph", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the temperature controller settings
	//	data:	{ 	"minimum": 20,
	//			"maximum": 25
	//		}
	getTemperatureSettings: function(callback) {
		$.get("/api/temperature/settings", function(data) {
			if (typeof callback == "function") {
				console.log("GET temperature settings:\nminimum: "+data.minimum+"\nmaximum: "+data.maximum);
				callback(data);
			}
		});
	},
	// 	post the temperature controller settings
	//	data:	{ 	"minimum": 20,
	//			"maximum": 25
	//		}
	// 	TODO res
	setTemperatureSettings: function(data, callback) {
		console.log("POST temperature settings:\nminimum: "+data.minimum+"\nmaximum: "+data.maximum);
		$.post("/api/temperature/settings", data, function(res) {
			if (typeof callback == "function") {
				callback(res);
			}
		});
	},
	// 	get the temperature controller status
	// 	data:	{
	//			"status": "[ok|warning|error]",
	//			"message": "some status message"
	//		}
	getTemperatureStatus: function(callback) {
		$.get("/api/temperature/status", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
		//console.log("TemperatureStatus entered...");
	},
	// 	test temperature controller
	// 	TODO data, res
	//	note: - Currently, the data from the post is ignored. The test runs whenever
	//		a http POST is received.
	testTemperature: function(data, callback) {
		$.post("/api/temperature/test", data, function(res) {
			if (typeof callback == "function") {
				callback(res);
			}
		});
	},
	/*
	 *	Humidity
	 */
	//	get the current humidity
	//	data: { "humidity" : 62.3 }
	getCurrentHumidity: function(callback) {
		$.get("/api/humidity/current", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the humidity graph data
	//	data: [{"humidity":62.3,"timestamp":1493012412553}]
	getHumidityGraphData: function(callback) {
		$.get("/api/humidity/graph", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the humidity controller settings
	//	data:	{ 	"minimum": 45,
	//			"maximum": 82,
	//			"interval": 360, (minutes)
	//			"duration": 12 (seconds)
	//		}
	getHumiditySettings: function(callback) {
		$.get("/api/humidity/settings", function(data) {
			if (typeof callback == "function") {
				console.log("GET humidity settings:\nminimum: "+data.minimum+"\nmaximum: "+data.maximum+"\ninterval: "+data.interval+"\nduration: "+data.duration);
				callback(data);
			}
		});
	},
	//	set the humidity controller settings
	//	data:	{ 	"minimum": 45,
	//			"maximum": 82,
	//			"interval": 360,
	//			"duration": 12
	//		}
	//	TODO res
	setHumiditySettings: function(data, callback) {
		$.post("/api/humidity/settings", data, function(res) {
			console.log("POST humidity settings:\nminimum: "+data.minimum+"\nmaximum: "+data.maximum+"\ninterval: "+data.interval+"\nduration: "+data.duration);
			if (typeof callback == "function") {
				callback(res);
			}
		});
	},
	// 	get the humidity controller status
	// 	data:	{
	//			"status": "[ok|warning|error]",
	//			"message": "some status message"
	//		}
	getHumidityStatus: function(callback) {
		$.get("/api/humidity/status", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	test humidity controller
	// 	TODO data, res
	//	note: - Currently, the data from the post is ignored. The test runs whenever
	//		a http POST is received.
	testHumidity: function(data) {
		$.post("/api/humidity/test", data, function(res) {
			if (typeof callback == "function") {
				callback(res);
			}
		});
	},
	/*
	 *	Lighting
	 */
	//	get the current lighting
	//	data: { "lighting" : 87 }
	getCurrentLighting: function(callback) {
		$.get("/api/lighting/current", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the lighting graph data
	//	data: [{"lighting":87,"timestamp":1493012412553}]
	getLightingGraphData: function(callback) {
		$.get("/api/lighting/graph", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the lighting controller settings
	//	data:	{ 	"sunrise": "09:00"
	//			"sunset": "22:30"
	//		}
	getLightingSettings: function(callback) {
		$.get("/api/lighting/settings", function(data) {
			if (typeof callback == "function") {
				console.log("GET lighting settings:\nsunrise: "+data.sunrise+"\nsunset: "+data.sunset);
				callback(data);
			}
		});
	},
	// 	set the lighting controller settings
	//	data:	{ 	"sunrise": "09:00"
	//			"sunset": "22:30"
	//		}
	//	TODO res
	setLightingSettings: function(data, callback) {
		console.log("POST lighting settings:\nsunrise: "+data.sunrise+"\nsunset: "+data.sunset);
		$.post("/api/lighting/settings", data, function(res) {
			if (typeof callback == "function") {
				callback(res);
			}
		});
	},
	// 	get the lighting controller status
	// 	data:	{
	//			"status": "[ok|warning|error]",
	//			"message": "some status message"
	//		}
	getLightingStatus: function(callback) {
		$.get("/api/lighting/status", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	test lighting controller
	// 	TODO data, res
	//	note: - Currently, the data from the post is ignored. The test runs whenever
	//		a http POST is received.
	testLighting: function(data, callback) {
		$.post("/api/lighting/test", data, function(res) {
			if (typeof callback == "function") {
				callback(res);
			}
		});
	},

	// get the current season from the api
	getCurrentSeason: function(callback) {
		$.get("/api/season/current", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	/*
	 *	Notifications
	 */
	//	get the list of notifications
	//	TODO data: []
	getNotifications: function(callback) {
		$.get("/api/notifications", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	//	get the list of Qcom Devices
	//	data:	[{},{},...,{}]
	getDeviceList: function(callback) {
		$.get("/api/system/devicelist", function(data) {
			if (typeof callback == "function") {
				callback(data);
			}
		});
	},
	// 	get the Controller's UUID
	//	data:	{ 	"UUID":"xxx-xxxxxx-xxxx"
	//		}
	getControllerUUID: function(callback) {
		$.get("/api/system/uuid", function(data) {
			if (typeof callback == "function") {
				console.log("Controller UUID: "+data.uuid);
				callback(data);
			}
		});
	}
};
