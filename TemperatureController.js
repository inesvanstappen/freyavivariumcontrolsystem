/*
 *	<some text is missing>
 *	written by:	Tom 'SpuQ' Santens
 *		on:	16/04/2019 - based on29/05/2017
 */

var EventEmitter = require('events').EventEmitter;
var util = require('util');

util.inherits(TemperatureController, EventEmitter);

module.exports = TemperatureController;

function TemperatureController(){
	var minimum;	// minimum and maximum settings
	var maximum;
	var current;	// value from sensor
	var state;	// heating/cooling/idle

	var noSensorMode = true; // presence of the sensor

	setInterval( regulator, 2000, this);

	this.setCurrent = function( value ){
		this.current = value;
	}

	this.settings = function( minTemperature, maxTemperature ){
		this.minimum = minTemperature;
		this.maximum = maxTemperature;
	}

	this.noSensor = function( value ){
		this.noSensorMode = value;
	}

	function regulator( tc ){
		// if a sensor is connected
		if( !tc.noSensorMode ){
			if( tc.current >= tc.maximum ){
				tc.state = "cooling";
				tc.emit("cooler", "on");
			}
			else if( tc.current <= tc.minimum ){
				tc.state = "heating";
				tc.emit("heater", "on");
			}
			else if( tc.current >= ((tc.maximum+tc.minimum)/2) && tc.state == "heating"){
				tc.state = "idle";
				tc.emit("heater", "off");
			}
			else if( tc.current <= ((tc.maximum+tc.minimum)/2) && tc.state == "cooling"){
				tc.state = "idle";
				tc.emit("cooler", "off");
			}
		}
		// if no sensor is connected
		else{
			tc.emit("cooler", "off");	// set heater and cooler off
			tc.emit("heater", "off");
			tc.state = "disabled: no sensor";
		}
	}
}

